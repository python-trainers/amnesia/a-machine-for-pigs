from trainerbase.gameobject import GameFloat
from trainerbase.memory import Address

from memory import player_coords_base_pointer


player_base_address = Address(player_coords_base_pointer)

player_x = GameFloat(player_base_address.inherit(extra_offsets=[0x48]))
player_z = GameFloat(player_base_address.inherit(extra_offsets=[0x4C]))
player_y = GameFloat(player_base_address.inherit(extra_offsets=[0x50]))
