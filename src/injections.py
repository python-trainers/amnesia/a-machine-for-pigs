from trainerbase.codeinjection import AllocatingCodeInjection, CodeInjection
from trainerbase.memory import pm

from memory import player_coords_base_pointer


update_player_coords_base_pointer = AllocatingCodeInjection(
    pm.base_address + 0x171A48,
    f"""
        mov [{player_coords_base_pointer}], esi
        fadd dword [esi + 0x48]
        fstp dword [esi + 0x48]
    """,
    original_code_length=6,
)


infinite_health = CodeInjection(pm.base_address + 0x10B099, b"\x90" * 6)
