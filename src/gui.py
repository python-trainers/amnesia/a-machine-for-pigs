from dearpygui import dearpygui as dpg
from trainerbase.gui import add_codeinjection_to_gui, add_teleport_to_gui, simple_trainerbase_menu

from injections import infinite_health
from teleport import tp


@simple_trainerbase_menu("A Machine for Pigs", 590, 265)
def run_menu():
    add_codeinjection_to_gui(infinite_health, "Infinite Health", "F1")
    dpg.add_separator()
    add_teleport_to_gui(tp, "Insert", "Home", "End")
